import Mock from '../mock';

const instanceDB = {
    instances: [
        {
          'id':1,  
          'partition': 'Partition 1',
          'index': '12345',
          'name': 'John',
          'something': 'Something',
          'eelse': 'Else',
          'type': 'Type 1',
          'desc': 'Desc 1',
          'region': 'Region 1'
        },
        {
          'id':2,  
          'partition': 'Partition 1',
          'index': '12345',
          'name': 'John',
          'something': 'Something',
          'eelse': 'Else',
          'type': 'Type 1',
          'desc': 'Desc 1',
          'region': 'Region 1'
        },
        {
          'id':3,  
          'partition': 'Partition 1',
          'index': '12345',
          'name': 'John',
          'something': 'Something',
          'eelse': 'Else',
          'type': 'Type 1',
          'desc': 'Desc 1',
          'region': 'Region 1'
        },
        {
          'id':4,  
          'partition': 'Partition 1',
          'index': '12345',
          'name': 'John',
          'something': 'Something',
          'eelse': 'Else',
          'type': 'Type 1',
          'desc': 'Desc 1',
          'region': 'Region 1'
        },
        {
          'id':5,  
          'partition': 'Partition 1',
          'index': '12345',
          'name': 'John',
          'something': 'Something',
          'eelse': 'Else',
          'type': 'Type 1',
          'desc': 'Desc 1',
          'region': 'Region 1'
        },
        {
          'id':6,  
          'partition': 'Partition 1',
          'index': '12345',
          'name': 'John',
          'something': 'Something',
          'eelse': 'Else',
          'type': 'Type 1',
          'desc': 'Desc 1',
          'region': 'Region 1'
        },
        {
          'id':7,  
          'partition': 'Partition 1',
          'index': '12345',
          'name': 'John',
          'something': 'Something',
          'eelse': 'Else',
          'type': 'Type 1',
          'desc': 'Desc 1',
          'region': 'Region 1'
        },
        {
          'id':8,  
          'partition': 'Partition 1',
          'index': '12345',
          'name': 'John',
          'something': 'Something',
          'eelse': 'Else',
          'type': 'Type 1',
          'desc': 'Desc 1',
          'region': 'Region 1'
        },
        {
          'id':9,  
          'partition': 'Partition 1',
          'index': '12345',
          'name': 'John',
          'something': 'Something',
          'eelse': 'Else',
          'type': 'Type 1',
          'desc': 'Desc 1',
          'region': 'Region 1'
        },
        {
          'id':10,  
          'partition': 'Partition 1',
          'index': '12345',
          'name': 'John',
          'something': 'Something',
          'eelse': 'Else',
          'type': 'Type 1',
          'desc': 'Desc 1',
          'region': 'Region 1'
        },
        {
          'id':11,  
          'partition': 'Partition 1',
          'index': '12345',
          'name': 'John',
          'something': 'Something',
          'eelse': 'Else',
          'type': 'Type 1',
          'desc': 'Desc 1',
          'region': 'Region 1'
        },
        {
          'id':12,  
          'partition': 'Partition 1',
          'index': '12345',
          'name': 'John',
          'something': 'Something',
          'eelse': 'Else',
          'type': 'Type 1',
          'desc': 'Desc 1',
          'region': 'Region 1'
        },
        {
          'id':13,  
          'partition': 'Partition 1',
          'index': '12345',
          'name': 'John',
          'something': 'Something',
          'eelse': 'Else',
          'type': 'Type 1',
          'desc': 'Desc 1',
          'region': 'Region 1'
        },
        {
          'id':14,  
          'partition': 'Partition 1',
          'index': '12345',
          'name': 'John',
          'something': 'Something',
          'eelse': 'Else',
          'type': 'Type 1',
          'desc': 'Desc 1',
          'region': 'Region 1'
        },
        {
          'id':15,  
          'partition': 'Partition 1',
          'index': '12345',
          'name': 'John',
          'something': 'Something',
          'eelse': 'Else',
          'type': 'Type 1',
          'desc': 'Desc 1',
          'region': 'Region 1'
        },
        {
          'id':16,  
          'partition': 'Partition 1',
          'index': '12345',
          'name': 'John',
          'something': 'Something',
          'eelse': 'Else',
          'type': 'Type 1',
          'desc': 'Desc 1',
          'region': 'Region 1'
        },
        {
          'id':17,  
          'partition': 'Partition 1',
          'index': '12345',
          'name': 'John',
          'something': 'Something',
          'eelse': 'Else',
          'type': 'Type 1',
          'desc': 'Desc 1',
          'region': 'Region 1'
        },
        {
          'id':18,  
          'partition': 'Partition 1',
          'index': '12345',
          'name': 'John',
          'something': 'Something',
          'eelse': 'Else',
          'type': 'Type 1',
          'desc': 'Desc 1',
          'region': 'Region 1'
        },
        {
          'id':19,  
          'partition': 'Partition 1',
          'index': '12345',
          'name': 'John',
          'something': 'Something',
          'eelse': 'Else',
          'type': 'Type 1',
          'desc': 'Desc 1',
          'region': 'Region 1'
        },
        {
          'id':20,  
          'partition': 'Partition 1',
          'index': '12345',
          'name': 'John',
          'something': 'Something',
          'eelse': 'Else',
          'type': 'Type 1',
          'desc': 'Desc 1',
          'region': 'Region 1'
        },
        {
          'id':21,  
          'partition': 'Partition 1',
          'index': '12345',
          'name': 'John',
          'something': 'Something',
          'eelse': 'Else',
          'type': 'Type 1',
          'desc': 'Desc 1',
          'region': 'Region 1'
        },
        {
          'id':22,  
          'partition': 'Partition 1',
          'index': '12345',
          'name': 'John',
          'something': 'Something',
          'eelse': 'Else',
          'type': 'Type 1',
          'desc': 'Desc 1',
          'region': 'Region 1'
        },
        {
          'id':23,  
          'partition': 'Partition 1',
          'index': '12345',
          'name': 'John',
          'something': 'Something',
          'eelse': 'Else',
          'type': 'Type 1',
          'desc': 'Desc 1',
          'region': 'Region 1'
        },
        {
          'id':24,  
          'partition': 'Partition 1',
          'index': '12345',
          'name': 'John',
          'something': 'Something',
          'eelse': 'Else',
          'type': 'Type 1',
          'desc': 'Desc 1',
          'region': 'Region 1'
        },
        {
          'id':25,  
          'partition': 'Partition 1',
          'index': '12345',
          'name': 'John',
          'something': 'Something',
          'eelse': 'Else',
          'type': 'Type 1',
          'desc': 'Desc 1',
          'region': 'Region 1'
        },
        {
          'id':26,  
          'partition': 'Partition 1',
          'index': '12345',
          'name': 'John',
          'something': 'Something',
          'eelse': 'Else',
          'type': 'Type 1',
          'desc': 'Desc 1',
          'region': 'Region 1'
        }
    ]
}

Mock.onGet('/api/instances/all').reply(config => {
  return [200, instanceDB.instances]
});

Mock.onGet('/api/instances').reply((config) => {
  const id = config.data;
  const response = instanceDB.instances.find((instance) => instance.id === id);
  return [200, response];
});

Mock.onPost('/api/instances/delete').reply(config => {
  let instance = JSON.parse(config.data);
  let index = { i: 0 };
  instanceDB.instances.forEach(element => {
      if (element.id === instance.id) {
          return [200, instanceDB.instances.splice(index.i, 1)]
      }
      index.i++
  });
  return [200, instanceDB.instances]
});

Mock.onPost('/api/instances/update').reply((config) => {
  let instance = JSON.parse(config.data);
  let index = { i: 0 };
  instanceDB.instances.forEach(element => {
      if (element.id === instance.id) {
        instanceDB.instances[index.i] = instance
          return [200, instanceDB.instances]
      }
      index.i++
  });
  return [200, instanceDB.instances]
});

Mock.onPost('/api/instances/add').reply((config) => {
  let instance = JSON.parse(config.data);
  instanceDB.instances.push(instance)
  return [200, instanceDB.instances]
});