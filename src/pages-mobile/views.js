
import React, { Component } from "react";
import { PageWrapper, DatabaseIdPan } from './style.js';
import { Button, InputGroup, FormControl } from 'react-bootstrap';
import { RiDatabase2Fill } from "react-icons/ri";
import DropdownViewEdit from '../components/DropdownViewEdit';
import '../css/talk-bubble.css';
import '../css/table.css';
import '../css/loading.css';
import SlidingPane from "react-sliding-pane";
import "react-sliding-pane/dist/react-sliding-pane.css";
import {databaseID} from '../components/DatabaseID';

import axios from "axios";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";

import { toast } from "react-toastify";
class ViewsPageMobile extends Component {
    constructor(props) {
        super(props);

        this.state = {
            instanceList: [],
            totalCount: 0,
            slideOpen: false,
            loaded: false
        }
    
        this.columns = [];
    
        this.options = {
            sizePerPage: 10,
            hideSizePerPage: true,
            hidePageListOnlyOnePage: true,
            showTotal: true,
        };
    }

    toggle(row) {
        document.getElementById(`dropdown-menu-${row.id}`).style.display = 'block'
    }

    handleSlideOpen() {
        const items = document.getElementsByClassName(`dropdown`);
        for (var idx = 0; idx < items.length; idx++) {
            items[idx].style.display = 'none'
        }
        this.setState({slideOpen: true});
    }

    handleSlideClose() {
        const items = document.getElementsByClassName(`dropdown`);
        for (var idx = 0; idx < items.length; idx++) {
            items[idx].style.display = 'block'
        }
        this.setState({slideOpen: false});
        this.refreshList();
    }

    handleAddView() {
        const name = document.getElementById('additem-name').value;
        const type = document.getElementById('additem-type').value;
        const partition = document.getElementById('additem-partition').value;
        const order = document.getElementById('additem-order').value;

        if (name === '' || type === '' || partition === '' || order === '') {
            toast.configure();
            toast.error("Please fill every items.", 4);
            return;
        }

        this.setState({slideOpen: false, loaded: false});
        this.addViewItem(name, type, partition, order);
    }

    refreshList() {  
        const queryAPI = `https://master.gyxi.com/${databaseID}/views`;
        
        axios.get(queryAPI).then(res => {
            const {data} = res;
            let instanceList = data;
            if (!this.state.loaded && instanceList.length > 0) {
                this.columns = [];
                const obj = Object.entries(data[0]);
                for (const [key/*, value*/] of obj) {
                    const headerItem = {
                        dataField: `${key}`,
                        text: `${key}`.toUpperCase(),
                        sort: false,
                        headerStyle: { color: '#617088' },
                        style: { color: '#617088' },
                        formatter: (cellContent, row) => {
                            if (key === 'orderByDescending') {
                                return (<div style={{justifyContent: 'space-between', display: 'flex'}}>
                                            {row.orderByDescending ? 'True': 'False'}
                                        </div>)
                            } else {
                                return (<div style={{justifyContent: 'space-between', display: 'flex'}}>
                                            {cellContent}
                                            {(obj[obj.length-1][0] === key) && <DropdownViewEdit item={row} object={this}/>}
                                        </div>)
                            }
                        } 
                    }
                    this.columns.push(headerItem);
                }
            }
            if (instanceList.length > 0)
                this.setState({ instanceList, loaded: true });
            else
                this.setState({ instanceList: [], loaded: true });
        });
    }

    addViewItem(name, type, partition, order) {
        let instance = {};
        let date = new Date()
        let id_string = `${date.getMinutes()}${date.getSeconds()}`
        instance.id = id_string;
        instance.name = name;
        instance.type = type;
        instance.partition = partition;
        instance.orderBy = order;
        
        const queryAPI = `https://master.gyxi.com/${databaseID}/views`;
        axios.post(queryAPI, instance).then(res => { 
            if (res.status === 200)          
                this.refreshList();
        });
    }
    
    updateDataItem(name, type, partition, order) {
        toast.configure();
        toast.info("This function is invalid yet.", 4)
        // this.setState({ loaded: false });

        // let instance = {};
        // instance.id = id;
        // instance.name = name;
        // instance.description = desc;
        
        // const queryAPI = `https://${this.state.location}-save.gyxi.com/${databaseID}/${this.state.type === '' ? 'type': this.state.type}/${this.state.partition === '' ? 'partition' : this.state.partition}/${id}`;
        // axios.post(queryAPI, instance).then(res => { 
        //     if (res.status === 200)          
        //         this.refreshList();
        // });
    }

    deleteDataItem(id) {
        toast.configure();
        toast.info("This function is invalid yet.", 4)
        // this.setState({ loaded: false });
        
        // const queryAPI = `https://${this.state.location}-delete.gyxi.com/${databaseID}/${this.state.type === '' ? 'type': this.state.type}/${this.state.partition === '' ? 'partition' : this.state.partition}/${id}`;
        // axios.delete(queryAPI).then(res => { 
        //     if (res.status === 200)          
        //         this.refreshList();
        // });
    }

    componentDidMount() {
        this.refreshList();
    }

    render() {
        let { 
          instanceList,
          slideOpen,
          loaded
        } = this.state;
        return (
            <PageWrapper>
                <div className="row">
                    <div className="col-md-6-mine padding-adjust">
                        <DatabaseIdPan>
                            <RiDatabase2Fill style={{marginLeft: '16px', marginRight: '16px', fontSize: '32px'}}/>Database ID: {databaseID}
                        </DatabaseIdPan>                    
                    </div>
                    <div className="offset-md-4-mine col-md-2-mine padding-adjust">
                    <Button variant="info" style={{width: '100%'}} onClick={()=>this.handleSlideOpen()}>Add View</Button>
                    </div>
                </div>
                <div id="page-data-table" style={{marginTop: '50px', marginBottom: '100px', color: '#617088'}}>
                    {loaded ? (instanceList.length > 0) ? <BootstrapTable
                        keyField="id"
                        striped
                        hover
                        bordered={ false }
                        data={ instanceList }
                        columns={ this.columns }
                        pagination={ paginationFactory(this.options) }
                    /> : <div style={{textAlign: 'center'}}><p>No views</p></div> : <div style={{textAlign: 'center'}}><span className="loader"></span><p>Loading...</p></div>}
                </div>
                <SlidingPane
                    isOpen={slideOpen}
                    title="ADD VIEW"
                    from="right"
                    width="250px"
                    onRequestClose={() => this.handleSlideClose()}
                >
                    <div className="row add-view-item-adjust">
                        <label htmlFor="index">Name</label>
                        <InputGroup style={{width: '100%'}}>
                            <InputGroup.Prepend/>
                            <FormControl  
                                id="additem-name"
                                className="border-radius-dual"
                                placeholder="Name"
                            />
                        </InputGroup>
                    </div>
                    <div className="row add-view-item-adjust">
                        <label htmlFor="type">Type</label>
                        <InputGroup style={{width: '100%'}}>
                            <InputGroup.Prepend/>
                            <FormControl  
                                id="additem-type"
                                className="border-radius-dual"
                                placeholder="Type"
                            />
                        </InputGroup>
                    </div>
                    <div className="row add-view-item-adjust">
                        <label htmlFor="partition">Partition By</label>
                        <InputGroup style={{width: '100%'}}>
                            <InputGroup.Prepend/>
                            <FormControl  
                                id="additem-partition"
                                className="border-radius-dual"
                                placeholder="Partition"
                            />
                        </InputGroup>
                    </div>
                    <div className="row add-view-item-adjust">
                        <label htmlFor="desc">Order By</label>
                        <InputGroup style={{width: '100%'}}>
                            <InputGroup.Prepend/>
                            <FormControl  
                                id="additem-order"
                                className="border-radius-dual"
                                placeholder="Order"
                            />
                        </InputGroup>
                    </div>
                    <br/>                    
                    <div className="row add-view-item-adjust">
                        <Button variant="info" style={{width: '100%'}} onClick={()=>{this.handleAddView()}}>Add</Button>
                    </div>
                    <Button variant="light" style={{width: '100%'}} onClick={()=>this.handleSlideClose()}>Cancel</Button>
                </SlidingPane>
            </PageWrapper>
        );
    }
}

export default ViewsPageMobile;

