
import React, { Component } from "react";
import { PageWrapper, DatabaseIdPan } from './style.js';
import { Form, InputGroup, FormControl, Button } from 'react-bootstrap';
import { RiDatabase2Fill } from "react-icons/ri";
import DropdownEdit from '../components/DropdownEdit';
import '../css/talk-bubble.css';
import '../css/loading.css';
import SlidingPane from "react-sliding-pane";
import "react-sliding-pane/dist/react-sliding-pane.css";
import {databaseID} from '../components/DatabaseID';

import axios from "axios";
import BootstrapTable from "react-bootstrap-table-next";
// import paginationFactory from "react-bootstrap-table2-paginator";

import { toast } from "react-toastify";
class DataPageMobile extends Component {
    constructor(props) {
        super(props);

        this.state = {
            instanceList: [],
            totalCount: 0,
            loaded: false,
            location: 'testregion',            
            slideOpen: false,
            numberid: '',
            type: '',
            partition: '',
        }

        this.columns = [];
        this.nextToken = [];
        this.nextToken[0] = null;
        this.nextToken[1] = null;
        this.nextTokenID = 1;
        
        this.options = {
            sizePerPage: 1000,
            hideSizePerPage: true,
            hidePageListOnlyOnePage: true,
            showTotal: true,
        };
    }

    toggle(row) {
        document.getElementById(`dropdown-menu-${row.id}`).style.display = 'block'
    }

    handleSlideOpen() {
        if (!this.checkTypePartition()) return;

        const items = document.getElementsByClassName(`dropdown`);
        for (var idx = 0; idx < items.length; idx++) {
            items[idx].style.display = 'none'
        }
        this.setState({slideOpen: true});
    }

    handleSlideClose() {
        const items = document.getElementsByClassName(`dropdown`);
        for (var idx = 0; idx < items.length; idx++) {
            items[idx].style.display = 'block'
        }
        this.setState({slideOpen: false});
    }

    checkTypePartition() {
        if (this.state.type === '' && this.state.partition === '') {
            toast.configure();
            toast.error("Please entry type and partition.", 4);
            return false;
        }        

        return true;
    }

    handleAddData() {
        const id = document.getElementById('additem-id').value;
        const json = document.getElementById('additem-json').value;


        if (id === '' || json === '') {
            toast.configure();
            toast.error("Please fill every items.", 4);
            return;
        }

        this.setState({slideOpen: false, loaded: false});
        this.addDataItem(id, json);
    }

    handleGo() {
        if (!this.checkTypePartition()) return;

        this.setState({loaded: false});
        this.refreshList(this.nextTokenID);
    }

    handleSelectLocation(e) {
        let location = "testregion";
        switch(e.target.value) {
            case 'TestRegion':
                location = "testregion";
                break;
            case 'Germany':
                location = "germany";
                break;
            default:
                break;
        }
        this.setState({location})
    }

    handleOnlyNumber(event) {
        const re = /^[0-9\b]+$/;
        if (event.target.value === '' || re.test(event.target.value)) {
            this.setState({numberid: event.target.value})
        }
    }

    handleChangeType(event) {
        this.setState({type: event.target.value})
    }

    handleChangePartition(event) {
        this.setState({partition: event.target.value})
    }

    refreshList(tokenid) {   
        const queryAPI = `https://${this.state.location}-list.gyxi.com/${databaseID}/${this.state.type === '' ? 'type': this.state.type}/${this.state.partition === '' ? 'partition' : this.state.partition}/${this.nextToken[this.nextTokenID - 1] ? this.nextToken[this.nextTokenID - 1] : ''}`;


        axios.get(queryAPI).then((res) => {
            const { data } = res;
            let tmpHeaderList = [];           
            let instanceList = data.result;
            this.nextToken[tokenid] = data.nextPageToken;
            if (!this.state.loaded && instanceList.length > 0) {
                this.columns = [];
                const firstHeaderItem = {
                    dataField: `partition`,
                    text: "PARTITION",
                    sort: false,
                    headerStyle: { color: '#617088' },
                    style: { color: '#617088' },
                    formatter: (cellContent, row) => (
                        <div>
                            {this.state.partition === '' ? 'partition' : this.state.partition}
                        </div>
                    )
                }
                this.columns.push(firstHeaderItem);
                data.result.forEach(obj_ => {
                    const obj = Object.entries(obj_);
                for (const [key/*, value*/] of obj) {
                        if (tmpHeaderList.includes(key)) continue;
                        tmpHeaderList.push(key);
                    const headerItem = {
                        dataField: `${key}`,
                        text: `${key}`.toUpperCase(),
                        sort: false,
                        headerStyle: { color: '#617088' },
                            style: { color: '#617088' }
                        }
                        this.columns.push(headerItem);
                    }
                })
                const endHeaderItem = {
                    dataField: `_ctl_`,
                    text: "",
                    sort: false,
                    headerStyle: { color: '#617088' },
                        style: { color: '#617088' },
                        formatter: (cellContent, row) => (
                        <div>
                            <DropdownEdit item={row} object={this}/>
                        </div>
                        )
                    }
                this.columns.push(endHeaderItem);
            }
            if (instanceList.length > 0)
                this.setState({ instanceList, loaded: true });
            else
                this.setState({ instanceList: [], loaded: true });
        }).catch(err=>{
            this.setState({ instanceList: [], loaded: true });
        });
    }

    
    addValueInObject(object, key, value) {
        var res = {};
        var textObject = JSON.stringify(object);
        if (textObject === '{}') {
            res = JSON.parse('{"' + key + '":"' + value + '"}');
        } else {
            res = JSON.parse('{' + textObject.substring(1, textObject.length - 1) + ',"' + key + '":"' + value + '"}');
        }
        return res;
    }

    addDataItem(id, json) {
        let instance = {};
        instance.id = id;    
        const splititems = json.split(",");
        splititems.forEach(item => {
            const splitpair = item.split(":");
            if (splitpair.length === 2) {
                let key = splitpair[0].replace(/[^a-zA-Z ]/g, "");
                let value = splitpair[1].replace(/[^a-zA-Z ]/g, "");
                instance = this.addValueInObject(instance, key, value);
            }
        });
        
        const queryAPI = `https://${this.state.location}-save.gyxi.com/${databaseID}/${this.state.type === '' ? 'type': this.state.type}/${this.state.partition === '' ? 'partition' : this.state.partition}/${id}`;
        axios.post(queryAPI, instance).then(res => { 
            if (res.status === 200)          
                this.refreshList(this.nextTokenID);
        });
    }

    updateDataItem(id, json) {
        this.setState({ loaded: false });

        let instance = {};
        instance.id = id;
        const splititems = json.split(",");
        splititems.forEach(item => {
            const splitpair = item.split(":");
            if (splitpair.length === 2) {
                let key = splitpair[0].replace(/[^a-zA-Z0-9]/g, "");
                let value = splitpair[1].replace(/[^a-zA-Z0-9]/g, "");
                instance = this.addValueInObject(instance, key, value);
            }
        });
        
        const queryAPI = `https://${this.state.location}-save.gyxi.com/${databaseID}/${this.state.type === '' ? 'type': this.state.type}/${this.state.partition === '' ? 'partition' : this.state.partition}/${id}`;
        axios.post(queryAPI, instance).then(res => { 
            if (res.status === 200)          
                this.refreshList(this.nextTokenID);
        });
    }

    deleteDataItem(id) {
        this.setState({ loaded: false });
        
        const queryAPI = `https://${this.state.location}-delete.gyxi.com/${databaseID}/${this.state.type === '' ? 'type': this.state.type}/${this.state.partition === '' ? 'partition' : this.state.partition}/${id}`;
        axios.delete(queryAPI).then(res => { 
            if (res.status === 200)          
                this.refreshList(this.nextTokenID);
        });
    }

    handlePrev() {
        this.setState({ loaded: false });
        this.nextTokenID--;
        this.refreshList(this.nextTokenID);
    }

    handleNext() {
        this.setState({ loaded: false });
        this.nextTokenID++;
        this.refreshList(this.nextTokenID);
    }

    componentDidMount() {
        this.refreshList(this.nextTokenID);
    }

    render() {
        let { 
          instanceList,
          loaded,
          numberid,
          slideOpen,
          type,
          partition
        } = this.state;
        return (
            <PageWrapper>
                <div className="row">
                    <div className="col-md-6-mine padding-adjust">
                        <DatabaseIdPan>
                            <RiDatabase2Fill style={{marginLeft: '16px', marginRight: '16px', fontSize: '32px'}}/>Database ID: {databaseID}
                        </DatabaseIdPan>                    
                    </div>                    
                    <div className="col-md-6-mine padding-adjust">
                        <Form.Group controlId="exampleForm.SelectCustom" style={{width: '100%'}}>
                            <Form.Control as="select" custom onChange={(e) => this.handleSelectLocation(e)}>
                                <option>TestRegion</option>
                                <option>Germany</option>
                            </Form.Control>
                        </Form.Group>
                    </div>
                    <div className="col-md-2-mine padding-adjust">
                    <InputGroup style={{width: '100%'}}>
                        <InputGroup.Prepend/>
                        <FormControl  
                            className="border-radius-dual"
                            placeholder="Type"
                            value={type}
                            onChange={(e) => this.handleChangeType(e)}
                        />
                    </InputGroup>
                    </div>
                    <div className="col-md-2-mine padding-adjust">
                    <InputGroup style={{width: '100%'}}>
                        <InputGroup.Prepend/>
                        <FormControl  
                            className="border-radius-dual"
                            placeholder="Partition"
                            value={partition}
                            onChange={(e) => this.handleChangePartition(e)}
                        />
                    </InputGroup>
                    </div>
                    <div className="col-md-1-mine padding-adjust">
                    <Button variant="info" style={{width: '100%'}} onClick={()=>this.handleGo()}>Go</Button>
                    </div>
                    <div className="col-md-1-mine padding-adjust">
                    <Button variant="info" style={{width: '100%'}} onClick={()=>this.handleSlideOpen()}>Add</Button>
                    </div>
                </div>
                <div id="page-data-table" style={{marginTop: '50px', color: '#617088'}}>
                    {loaded ? (instanceList.length > 0) ? <BootstrapTable
                        keyField="id"
                        striped
                        hover
                        bordered={ false }
                        data={ instanceList }
                        columns={ this.columns }
                        //pagination={ paginationFactory(this.options) }
                    /> : <div style={{textAlign: 'center'}}><p>No views</p></div> : <div style={{textAlign: 'center'}}><span className="loader"></span><p>Loading...</p></div>}
                </div>
                <div className="padding-adjust" style={{marginBottom: '100px'}}>
                    {this.nextToken[this.nextTokenID - 1] && <Button variant="primary" style={{width: '100px', margin: '5px 10px 0 0'}} onClick={()=>this.handlePrev()}>Prev</Button>}
                    {this.nextToken[this.nextTokenID] && <Button variant="primary" style={{width: '100px', margin: '5px 0 0 0'}} onClick={()=>this.handleNext()}>Next</Button>}
                </div>
                <SlidingPane
                    isOpen={slideOpen}
                    title="ADD DATA"
                    from="right"
                    width="250px"
                    onRequestClose={() => this.handleSlideClose()}
                >
                    <div className="row add-view-item-adjust">
                        <label htmlFor="type">ID</label>
                        <InputGroup style={{width: '100%'}}>
                            <InputGroup.Prepend/>
                            <FormControl  
                                id="additem-id"
                                className="border-radius-dual"
                                placeholder="123"
                                value={numberid}
                                onChange={(e)=>this.handleOnlyNumber(e)}
                            />
                        </InputGroup>
                    </div>
                    <div className="row add-view-item-adjust">
                        <label htmlFor="partition">JSON Data</label>
                        <Form.Control id="additem-json" as="textarea" rows={10} style={{resize:'none'}}/>
                    </div>                   
                    <br/>                    
                    <div className="row add-view-item-adjust">
                        <Button variant="info" style={{width: '100%'}} onClick={()=>{this.handleAddData();}}>Add</Button>
                    </div>
                    <Button variant="light" style={{width: '100%'}} onClick={()=>this.handleSlideClose()}>Cancel</Button>
                </SlidingPane>
            </PageWrapper>
        );
    }
}

export default DataPageMobile;

