
import React, { Component } from "react";
import { PageWrapper, DatabaseIdPan, SettingItemBar, SettingUpgradeMobileLabel, SettingUpgradeMobileControl } from './style.js';
import { RiDatabase2Fill } from "react-icons/ri";
import { Button } from 'react-bootstrap';
import Checkbox from '@material-ui/core/Checkbox';
import { toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import {databaseID} from '../components/DatabaseID';

class SettingsPageMobile extends Component {
    // constructor(props) {
    //     super(props)
    // }

    handleAlwaysCheck() {
        toast.configure()
        toast.error("Always Fast cannot be disabled. Gyxi is always fast.", 4);
    }

    handleIntantCheck() {
        toast.configure()
        toast.error("This is a premium future and is only available in Gyxi Pro", 4);
    }

    handleAPIFunction() {
        toast.configure()
        toast.info("API Key can make your database more secure. This feature is only available in Gyxi Pro.", 4);
    }

    handleDistributeFunction() {
        toast.configure()
        toast.info("You can distribute your data to other regions by creating views.", 4);
    }

    render() {
        return (
            <PageWrapper>
                <div className="row">
                    <div className="col-md-6-mine padding-adjust">
                        <DatabaseIdPan>
                            <RiDatabase2Fill style={{marginLeft: '16px', marginRight: '16px', fontSize: '32px'}}/>Database ID: {databaseID}
                        </DatabaseIdPan>                    
                    </div>
                </div>
                <div className="row" style={{marginTop: '50px'}}>
                    <SettingItemBar>
                        <span>Enable "Always Fast"</span>
                        <Checkbox checked={true} onClick={()=>this.handleAlwaysCheck()}></Checkbox>
                    </SettingItemBar>
                    <SettingItemBar style={{marginTop: '-1px'}}>
                        <span>Enable "Intant Views"</span>
                        <Checkbox checked={false} onClick={()=>this.handleIntantCheck()}></Checkbox>
                    </SettingItemBar>
                    <SettingItemBar style={{marginTop: '-1px'}}>
                        <span>API Key</span>
                        <Button variant="info" style={{width: '150px'}} onClick={()=>this.handleAPIFunction()}>Generate</Button>
                    </SettingItemBar>
                    <SettingItemBar style={{marginTop: '-1px'}}>
                        <span>Distribute to Regions</span>
                        <Button variant="info" style={{width: '150px'}} onClick={()=>this.handleDistributeFunction()}>Distribute</Button>
                    </SettingItemBar>
                </div>
                <div className="row">
                    <SettingUpgradeMobileLabel>
                        <img src="../settingupload.png" alt="logo" width="205px" height="140px"style={{marginLeft: '16px'}}/>
                        <div className="setting-upgrade-mobile">
                            <strong>Upgrade</strong>
                            <p>Upgrade to Gyxi Pro now to unlock more features</p>
                        </div>                     
                    </SettingUpgradeMobileLabel>
                    <SettingUpgradeMobileControl>
                        <a href={`mailto:nb@gyxi.com?subject=Upgrade ${databaseID}`}><Button variant="info" style={{width: '220px', height: '38.24px', margin: '10px 10px 10px 80px'}}>Upgrade</Button></a>
                    </SettingUpgradeMobileControl>
                </div>
            </PageWrapper>
        );
    }
}

export default SettingsPageMobile;

