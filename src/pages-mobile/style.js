import styled from 'styled-components';

export const SlideWidth = ((360 / window.innerWidth) * 100 - 13);

export const PageWrapper = styled.div`
  margin-left: 20px;
  padding-top: 70px;
  margin-right: 20px;
`;

export const DatabaseIdPan = styled.div`
  width: 100%;
  height: fit-content;
  background: #DEDEDE;
  color: #617088;
  align-items: center;
  display: flex;
  border-radius: 0.25rem !important;
  padding: 10px 0;
`;

export const BuildCommandPan = styled.div`
  width: 100%;
  height: fit-content;
  background: #DEDEDE;
  color: #617088;
  align-items: center;
  display: flex;
  border-radius: 0.25rem !important;
  padding: 10px 20px 10px 20px;
  margin-left: 0px;
`;

export const SettingItemBar = styled.div`
  width: 100%;
  height: 72px;
  color: #617088;
  align-items: center;
  display: flex;
  border-top: 1px solid #eaebed;
  border-bottom: 1px solid #eaebed;
  justify-content: space-between;
  padding: 0 20px 0 20px;
`;

export const SettingUpgrade = styled.div`
    width: 100%;
    height: 140px;
    display: flex;
    text-align: center;
    margin: 12px;
    background: #f4f7ff;;
    margin-top: 200px;
    border-radius: 6px;
`;
export const SettingUpgradeMobileLabel = styled.div`
    width: 100%;
    height: 140px;
    display: flex;
    text-align: center;
    margin: 12px;
    background: #f4f7ff;;
    margin-top: 200px;
    border-radius: 6px;
`;

export const SettingUpgradeMobileControl = styled.div`
    width: 100%;
    height: 60px;
    display: flex;
    text-align: center;
    margin: 12px;
    background: #f4f7ff;
    border-radius: 6px;
`;