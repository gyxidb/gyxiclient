
import React, { Component } from "react";
import { PageWrapper, DatabaseIdPan, BuildCommandPan } from './style.js';
import { RiDatabase2Fill } from "react-icons/ri";
import { Form, InputGroup, FormControl } from 'react-bootstrap';
import {databaseID} from '../components/DatabaseID';

class ApiPageMobile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            location: 'testregion',
            type: '',
            partition: '',
            id: '',
        }
    }

    handleSelectLocation(e) {
        let location = "testregion";
        switch(e.target.value) {
            case 'TestRegion':
                location = "testregion";
                break;
            case 'Germany':
                location = "germany";
                break;
            default:
                break;
        }
        this.setState({location})
    }

    handleChangeType(event) {
        this.setState({type: event.target.value})
    }

    handleChangePartition(event) {
        this.setState({partition: event.target.value})
    }

    handleChangeId(event) {
        this.setState({id: event.target.value})
    }

    render() {
        const {
            location,
            type,
            partition,
            id
        } = this.state;
        return (
            <PageWrapper>
                <div className="row">
                    <div className="col-md-6-mine padding-adjust">
                        <DatabaseIdPan>
                            <RiDatabase2Fill style={{marginLeft: '16px', marginRight: '16px', fontSize: '32px'}}/>Database ID: {databaseID}
                        </DatabaseIdPan>                    
                    </div>
                </div>
                <div className="row" style={{marginTop: '50px', display: 'block'}}>
                    <strong style={{fontSize: '20px', color: '#617088', marginBottom: '10px', width: '100%'}}>Build URLs</strong>
                    <BuildCommandPan className="row" style={{width:'100%'}}>
                        <div className="col-xd-2-mine">
                        <span style={{width: '100%'}}>https://action-</span>
                        </div>                        
                        <div className="col-xd-2-mine">
                        <Form.Group controlId="exampleForm.SelectCustom" style={{width: '100%', margin: '0 0 0 0' }}>
                            <Form.Control as="select" custom onChange={(e) => this.handleSelectLocation(e)}>
                                <option>TestRegion</option>
                                <option>Germany</option>
                            </Form.Control>
                        </Form.Group>
                        </div>                        
                        <div className="col-xd-3-mine">
                        <span style={{width: '100%'}}>.gyxi.com/{databaseID}</span>      
                        </div>
                        <div className="col-xd-2-mine">
                        <span style={{marginTop: '8px'}}>/</span>
                        <InputGroup style={{width: '100%', margin: '0 5px 0 5px'}}>
                            <InputGroup.Prepend/>
                            <FormControl  
                                className="border-radius-dual"
                                placeholder="Type"
                                value={type}
                                onChange={(e) => this.handleChangeType(e)}
                            />
                        </InputGroup>
                        </div>   
                        <div className="col-xd-2-mine">        
                        <span style={{marginTop: '8px'}}>/</span>
                        <InputGroup style={{width: '100%', margin: '0 5px 0 5px'}}>
                            <InputGroup.Prepend/>
                            <FormControl  
                                className="border-radius-dual"
                                placeholder="Partition"
                                value={partition}
                                onChange={(e) => this.handleChangePartition(e)}
                            />
                        </InputGroup>
                        </div>  
                        <div className="col-xd-1-mine">      
                        <span style={{marginTop: '8px'}}>/</span>
                        <InputGroup style={{width: '100%', margin: '0 5px 0 5px'}}>
                            <InputGroup.Prepend/>
                            <FormControl  
                                className="border-radius-dual"
                                placeholder="ID"
                                value={id}
                                onChange={(e) => this.handleChangeId(e)}
                            />
                        </InputGroup>
                        </div>  
                    </BuildCommandPan>
                </div>
                <div style={{marginTop: '80px'}}>
                    <div id="save" className="row padding-adjust-big">
                        <div className="col-md-2" style={{marginTop: '6px', color: '#617088'}}>                                                                                                         
                            <span style={{fontSize: '20px'}}>①</span>
                            <strong style={{fontSize: '20px', marginLeft: '10px'}}>Save</strong>
                        </div>
                        <div className="col-md-10">
                            <DatabaseIdPan style={{paddingLeft: '20px'}}>
                                https://{location}-save.gyxi.com/{databaseID}/{type===''?'type':type}/{partition===''?'partition':partition}/{id===''?'id':id}
                            </DatabaseIdPan>
                        </div>
                    </div>                    
                    <div id="get" className="row padding-adjust-big">
                        <div className="col-md-2" style={{marginTop: '6px', color: '#617088'}}>                                                                                                         
                            <span style={{fontSize: '20px'}}>②</span>
                            <strong style={{fontSize: '20px', marginLeft: '10px'}}>Get</strong>
                        </div>
                        <div className="col-md-10">
                            <DatabaseIdPan style={{paddingLeft: '20px'}}>
                                https://{location}-get.gyxi.com/{databaseID}/{type===''?'type':type}/{partition===''?'partition':partition}/{id===''?'id':id}
                            </DatabaseIdPan>
                        </div>
                    </div>                 
                    <div id="list" className="row padding-adjust-big">
                        <div className="col-md-2" style={{marginTop: '6px', color: '#617088'}}>                                                                                                         
                            <span style={{fontSize: '20px'}}>③</span>
                            <strong style={{fontSize: '20px', marginLeft: '10px'}}>List</strong>
                        </div>
                        <div className="col-md-10">
                            <DatabaseIdPan style={{paddingLeft: '20px'}}>
                                https://{location}-list.gyxi.com/{databaseID}/{type===''?'type':type}/{partition===''?'partition':partition}
                            </DatabaseIdPan>
                        </div>
                    </div>              
                    <div id="delete" className="row padding-adjust-big">
                        <div className="col-md-2" style={{marginTop: '6px', color: '#617088'}}>                                                                                                         
                            <span style={{fontSize: '20px'}}>④</span>
                            <strong style={{fontSize: '20px', marginLeft: '10px'}}>Delete</strong>
                        </div>
                        <div className="col-md-10">
                            <DatabaseIdPan style={{paddingLeft: '20px'}}>
                                https://{location}-delete.gyxi.com/{databaseID}/{type===''?'type':type}/{partition===''?'partition':partition}/{id===''?'id':id}
                            </DatabaseIdPan>
                        </div>
                    </div>            
                    <div id="batch" className="row padding-adjust-big">
                        <div className="col-md-2" style={{marginTop: '6px', color: '#617088'}}>                                                                                                         
                            <span style={{fontSize: '20px'}}>⑤</span>
                            <strong style={{fontSize: '20px', marginLeft: '10px'}}>Batch</strong>
                        </div>
                        <div className="col-md-10">
                            <DatabaseIdPan style={{paddingLeft: '20px'}}>
                                https://{location}-delete.gyxi.com/{databaseID}/{type===''?'type':type}/{partition===''?'partition':partition}
                            </DatabaseIdPan>
                        </div>
                    </div>
                </div>
            </PageWrapper>
        );
    }
}

export default ApiPageMobile;

