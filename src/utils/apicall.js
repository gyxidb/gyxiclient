import qs from 'qs'

const apicall = ({url, params, file, headers, method='GET', type='json'}) => {
  return new Promise(function(resolve, reject) {
    let request = new XMLHttpRequest();
    let token = window.localStorage.getItem("token")
    let full_url = type === "form" ? `${url}${params ? '?' : ''}${qs.stringify(params)}` : url
    request.open(method,  full_url, true);

    //Required headers
    request.setRequestHeader("Accept","application/json")
    request.setRequestHeader("Content-Type","application/json")
    request.setRequestHeader("token", token);

    if(Array.isArray(headers)){
      headers.map(([key,value]) => request.setRequestHeader(key, value) )    
    }

    request.onloadend = () => {
        resolve(request);
    };
   
    if(file){
      request.send(file)
    }else{
      request.send( type === 'json' ? JSON.stringify(params) : "");
    }
  })
}
export default apicall
