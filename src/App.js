import React, {useState} from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './css/index.css';
import {databaseID} from './components/DatabaseID';
import {isMobile} from 'react-device-detect';

import DataPage from './pages/data.js';
import ViewsPage from './pages/views.js';
import ApiPage from './pages/api.js';
import SettingsPage from './pages/settings.js';
import DataPageMobile from './pages-mobile/data.js';
import ViewsPageMobile from './pages-mobile/views.js';
import ApiPageMobile from './pages-mobile/api.js';
import SettingsPageMobile from './pages-mobile/settings.js';
import Sidebar from './components/Sidebar';
import Navigationbar from './components/Navigationbar/index';
require('./fake-db');
const limitWidth = 820;
function App() {
  const [response, setResponse] = useState(window.innerWidth < limitWidth ? true : false);
  React.useEffect(() => {
    function handleResize() {
      setResponse(window.innerWidth < limitWidth ? true : false);
    }
    window.addEventListener('resize', handleResize);
    return () => { window.removeEventListener('resize', handleResize) }
  })
  return (
    <React.Fragment>
      {(isMobile || response) && <Router>
        <Navigationbar/>
        <Switch>
          <Route exact path={`/${databaseID}`} component={DataPageMobile} />
          <Route path={`/${databaseID}/views`} component={ViewsPageMobile} />
          <Route path={`/${databaseID}/api`} component={ApiPageMobile} />
          <Route path={`/${databaseID}/settings`} component={SettingsPageMobile} />
        </Switch>
      </Router>}
      {(!isMobile && !response) && <Router>
        <Sidebar/> 
        <Switch>
          <Route exact path={`/${databaseID}`} component={DataPage} />
          <Route path={`/${databaseID}/views`} component={ViewsPage} />
          <Route path={`/${databaseID}/api`} component={ApiPage} />
          <Route path={`/${databaseID}/settings`} component={SettingsPage} />
        </Switch>
      </Router>}
    </React.Fragment>
  );
}

export default App;
