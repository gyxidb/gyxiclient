import React, { Component } from 'react';
import { withRouter } from 'react-router-dom'
import styled from "styled-components";
import { FiMoreVertical } from "react-icons/fi"
import { InputGroup, FormControl, Button, Modal } from 'react-bootstrap';
import SlidingPane from "react-sliding-pane";
import "react-sliding-pane/dist/react-sliding-pane.css";
import { toast } from "react-toastify";

class DropdownViewEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      slideOpen: false,
      modalOpen: false,
      name: props.item.name,
      type: props.item.type,
      partition: props.item.partitionBy,
      order: props.item.orderBy
    };
  }

  handleSlideOpen() {
    const items = document.getElementsByClassName(`dropdown`);
    for (var idx = 0; idx < items.length; idx++) {
      items[idx].style.display = 'none'
    }
    this.setState({slideOpen: true});
  }

  handleSlideClose() {
    const items = document.getElementsByClassName(`dropdown`);
    for (var idx = 0; idx < items.length; idx++) {
      items[idx].style.display = 'block'
    }
    this.setState({slideOpen: false});
  }

  handleUpdateData() {
    const name = document.getElementById('edititem-name').value;
    const type = document.getElementById('edititem-type').value;
    const partition = document.getElementById('edititem-partition').value;
    const order = document.getElementById('edititem-order').value;

    if (name === '' || type === '' || partition === '' || order === '') {
        toast.configure();
        toast.error("Please fill every items.", 4);
        return;
    }

    this.setState({slideOpen: false});
    this.props.object.updateDataItem(name, type, partition, order);
  }

  handleModalOpen() {
    this.setState({modalOpen: true});
  }

  handleModalClose() {
    this.setState({modalOpen: false});
  }

  handleDeleteRow() {
    this.setState({modalOpen: false});
    this.props.object.deleteDataItem(this.state.name);
  }

  render() {
    const {slideOpen, modalOpen, name, type, partition, order} = this.state;
    let modalClose = () => this.setState({ modalOpen: false });
    return (
      <DDE>
        <div className="dropdown" style={{width: "50px", textAlign: "center"}}>
          <FiMoreVertical style={{marginTop: '-4px', fontWeight: 'bold'}} className="table-edit" />
          <div className={`dropdown-content talk-bubble tri-right border round ${this.props.flagLastest ? 'right-bottom' : 'right-top'}`}>
            <div className="talktext">
              <p style={{color: '#617088'}} onClick={()=>this.handleSlideOpen()}>Edit</p>
            </div>
            <div className="talktext">
              <p style={{color: '#617088'}} onClick={()=>this.handleModalOpen()}>Delete</p>
            </div>
          </div>
        </div>
        <SlidingPane
          isOpen={slideOpen}
          title="EDIT DATA"
          from="right"
          width="250px"
          onRequestClose={() => this.handleSlideClose()}
        >
          <div className="row add-view-item-adjust">
            <label htmlFor="edititem-name">Name</label>
            <InputGroup style={{width: '100%'}}>
              <InputGroup.Prepend/>
              <FormControl  
                id="edititem-name"
                className="border-radius-dual"
                placeholder="Name"
                defaultValue={name}
              />
            </InputGroup>
          </div>  
          <div className="row add-view-item-adjust">
            <label htmlFor="edititem-type">Type</label>
            <InputGroup style={{width: '100%'}}>
              <InputGroup.Prepend/>
              <FormControl  
                id="edititem-type"
                className="border-radius-dual"
                placeholder="Type"
                defaultValue={type}
              />
            </InputGroup>
          </div>   
          <div className="row add-view-item-adjust">
            <label htmlFor="edititem-partition">Partition By</label>
            <InputGroup style={{width: '100%'}}>
              <InputGroup.Prepend/>
              <FormControl  
                id="edititem-partition"
                className="border-radius-dual"
                placeholder="Partition"
                defaultValue={partition}
              />
            </InputGroup>
          </div>  
          <div className="row add-view-item-adjust">
            <label htmlFor="edititem-order">Order By</label>
            <InputGroup style={{width: '100%'}}>
              <InputGroup.Prepend/>
              <FormControl  
                id="edititem-order"
                className="border-radius-dual"
                placeholder="Order"
                defaultValue={order}
              />
            </InputGroup>
          </div>                 
          <br/>                    
          <div className="row add-view-item-adjust">
            <Button variant="info" style={{width: '100%'}} onClick={()=>{this.handleUpdateData();}}>Update</Button>
          </div>
          <Button variant="light" style={{width: '100%'}} onClick={()=>this.handleSlideClose()}>Cancel</Button>
        </SlidingPane>
        <Modal
          show={modalOpen}
          onHide={modalClose}
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Name = {name} Item
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h5>Do you really want to delete it?</h5>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="danger" onClick={() => this.handleDeleteRow()}>Delete</Button>
            <Button onClick={() => this.handleModalClose()}>Close</Button>
          </Modal.Footer>
        </Modal>
      </DDE>
    );
  }
}

export const DDE = styled.div`

.profile-icon{
    font-size: 16px;
    border: none;
    cursor: pointer;
    background-color: transparent;
  }
  
  .dropdown {
    position: relative;
    display: inline-block;
    z-index: 10;
  }
  
  .dropdown-content {
    cursor: pointer;
    display: none;
    position: absolute;
    right: 32px;
    background-color: #f9f9f9;
    min-width: 120px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 100;
    text-align: left;
  }
  
  .dropdown-content p {
    color: black;
    padding: 5px 16px;
    text-decoration: none;
    display: block;
  }
  
  .dropdown-content p:hover {
    background-color: #f1f1f1;
    border-radius: 10px;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
  }
  
  .dropdown:hover .dropdown-content {
    display: block;
    z-index: 100;
  }
  
  .dropdown:hover .profile-icon {
  
  }

  .talkbubble {
    width: 120px;
    height: 80px;
    background: red;
    position: relative;
    -moz-border-radius: 10px;
    -webkit-border-radius: 10px;
    border-radius: 10px;
  }
  .talkbubble:before {
    content: "";
    position: absolute;
    right: 100%;
    top: 26px;
    width: 0;
    height: 0;
    border-top: 13px solid transparent;
    border-right: 26px solid red;
    border-bottom: 13px solid transparent;
  }
  
  `;

export default withRouter(DropdownViewEdit);