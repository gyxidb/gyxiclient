import React from 'react';
import styled from 'styled-components';
import { withRouter } from "react-router-dom";
import { RiDatabase2Fill } from "react-icons/ri";
import { AiFillEye } from "react-icons/ai";
import { IoExtensionPuzzle, IoSettingsSharp } from "react-icons/io5";
import { HiArrowNarrowRight } from "react-icons/hi";
import Button from 'react-bootstrap/Button';
import {Modal, InputGroup, FormControl} from 'react-bootstrap';
import {databaseID, setDatabaseID, pathlink} from './DatabaseID';
import { toast } from "react-toastify";

const UpgradePotion = Math.max(300, window.innerHeight - 200);

const StyledSideNav = styled.div`   
    position: fixed;     /* Fixed Sidebar (stay in place on scroll and position relative to viewport) */
    height: 100%;
    width: 300px;     /* Set the width of the sidebar */
    z-index: 1;      /* Stay on top of everything */
    overflow-x: hidden;     /* Disable horizontal scroll */
    background: #ffffff;
    box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;
`;

const SidebarTitle = styled.div`
    display: flex;
    width: 100%;
    height: 100px;
    padding: 10px;
    margin-left: 5px;
    margin-top: 20px;
`;

const SidebarItem = styled.div`
    width: 90%;
    height: 42px;
    padding: 10px;
    border-radius: 6px;
    margin-left: 5px;
    display:'flex';
`;

const SidebarUpgrade = styled.div`
    display: flex;
    position: absolute;
    width: 90%;
    height: 140px;
    text-align: center;
    margin: 12px;
    background: #EDEDED;
    top: ${ UpgradePotion }px;
    border-radius: 6px;
`;

const SidebarDrawerItem = (props) => {
    const { path, name, icon, active } = props;
    return (
        <a href={path}>
            <SidebarItem className={path === active ?"side-button-select":"side-button"}>
                <div id={`side-bar-icon-${name}`} style={{display:'flex', position:'absolute', left: '5%', padding:6, marginTop: '-7px', fontSize: '24px'}}>
                    {icon}
                </div>
                <div id={`side-bar-name-${name}`} style={{display:'flex', position:'absolute', left: '22%', padding:6, marginTop: '-6px'}}>
                    {name}
                </div>
                <div id={`side-bar-arrow-${name}`} style={{display:'flex', position:'absolute', backgroundColor: 'rgba(0,0,0,0.15)', visibility: `${path === active ? 'visible' : 'hidden'}`, right: '15%', padding:6, marginTop: '-3px', borderRadius: '3px'}}>
                    <HiArrowNarrowRight/>
                </div>
            </SidebarItem>
        </a>
    );
};

class SideNav extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activePath: props.location.pathname,
            show: false,
            dbid: '',
            items: [
                {
                  path: `/${databaseID}`, /* path is used as id to check which NavItem is active basically */
                  name: 'Data',
                  key: 0, /* Key is required, else console throws error. Does this please you Mr. Browser?! */
                  icon: <RiDatabase2Fill/>
                },
                {
                  path: `/${databaseID}/views`,
                  name: 'Views',
                  key: 1,
                  icon: <AiFillEye/>
                },
                {
                  path: `/${databaseID}/api`,
                  name: 'API',
                  key: 2,
                  icon: <IoExtensionPuzzle/>
                },
                {
                  path: `/${databaseID}/settings`,
                  name: 'Settings',
                  key: 3,
                  icon: <IoSettingsSharp/>
                },
            ]
        }
    }

    componentDidMount () {
        if (databaseID === '') {
            this.setState({show: true});
        }
    }

    onItemClick = (path) => {
        if (path === this.state.activePath) return;
        
        this.setState({ activePath: path });
    }

    handleClose = () => {
        if (this.state.dbid === '') {
            toast.configure()
            toast.error("Please input databaseID.", 4);
        } else {
            setDatabaseID(this.state.dbid);
            var items = [
                {
                  path: `/${this.state.dbid}`, /* path is used as id to check which NavItem is active basically */
                  name: 'Data',
                  key: 0, /* Key is required, else console throws error. Does this please you Mr. Browser?! */
                  icon: <RiDatabase2Fill/>
                },
                {
                  path: `/${this.state.dbid}/views`,
                  name: 'Views',
                  key: 1,
                  icon: <AiFillEye/>
                },
                {
                  path: `/${this.state.dbid}/api`,
                  name: 'API',
                  key: 2,
                  icon: <IoExtensionPuzzle/>
                },
                {
                  path: `/${this.state.dbid}/settings`,
                  name: 'Settings',
                  key: 3,
                  icon: <IoSettingsSharp/>
                },
            ]
            this.setState({show: false, items: items, activePath: `/${this.state.dbid}`});
            window.location.href = pathlink + `${this.state.dbid}`;
        }        
    }

    handleNothing = () => {
        
    }

    handleChangeDbID = (e) => {
        this.setState({dbid: e.target.value});
    }

    render() {
        const { items, activePath } = this.state;
        return(
            <div>
                <StyledSideNav>
                <SidebarTitle>
                    <img src={`../logo.png`} alt="logo" width="42px" height="41px"/>
                    <div className="side-title">Gyxi</div>
                </SidebarTitle>                
                {
                    items.map((item) => {
                        return (           
                            <SidebarDrawerItem 
                                key = {item.key}
                                name = {item.name} 
                                path = {item.path}
                                icon = {item.icon} 
                                active={activePath} 
                                onClick={() => this.onItemClick}
                            />
                        );
                    })
                }
                <SidebarUpgrade>
                    <div className="side-upgrade">
                        <strong>Upgrade</strong>
                        <p>Upgrade to Gyxi Pro</p>
                        <a href={`mailto:nb@gyxi.com?subject=Upgrade ${databaseID}`}><Button variant="info" style={{width: '150px'}}>Upgrade</Button></a>
                    </div>
                </SidebarUpgrade>
                </StyledSideNav>
                <Modal show={this.state.show} onHide={this.handleNothing} centered>
                    <Modal.Header>
                    <Modal.Title>Please input databaseID.</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <InputGroup style={{width: '100%'}}>
                            <InputGroup.Prepend/>
                            <FormControl  
                                className="border-radius-dual"
                                placeholder="DatabaseID"
                                value={this.state.dbid}
                                onChange={(e) => this.handleChangeDbID(e)}
                            />
                        </InputGroup>
                    </Modal.Body>
                    <Modal.Footer>
                    <Button variant="primary" onClick={this.handleClose}>
                        Confirm
                    </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

const RouterSideNav = withRouter(SideNav);

export default class Sidebar extends React.Component {
    render() {
        return (
            <RouterSideNav></RouterSideNav>
        );
    }
}