
const path = window.location.href;
const splitpath = path.split('/');
const query = splitpath[splitpath.length - 1];

if (query.toLowerCase() !== "views" && query.toLowerCase() !== "api" && query.toLowerCase() !== "settings") {
    // const guid = (query.toLowerCase() === "") ? splitpath[splitpath.length - 2] : splitpath[splitpath.length - 1];
    const guid = splitpath[splitpath.length - 1];
    //if (guid.includes("-")) {
    //if (guid !== "") {
        localStorage.setItem("guid", guid);
    //}
}
export let databaseID = localStorage.getItem("guid");//"5c2c7379-d444-48b2-8c2b-cf50d1c04348";
export const pathlink = databaseID === '' ? path : ''
export const setDatabaseID = (id) => {
    databaseID = id;
    localStorage.setItem("guid", id);
}