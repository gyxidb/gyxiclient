import React, { Component } from 'react';
import { withRouter } from 'react-router-dom'
import styled from "styled-components";
import { FiMoreVertical } from "react-icons/fi"
import { Form, Button, Modal } from 'react-bootstrap';
import SlidingPane from "react-sliding-pane";
import "react-sliding-pane/dist/react-sliding-pane.css";
import { toast } from "react-toastify";

class DropdownEdit extends Component {
  constructor(props) {
    super(props);
    this.numberid = props.item.id;
    this.json_ = "{\r\n"
    for (const [key, value] of Object.entries(this.props.item)) {
      if (key === 'id') continue;      
      this.json_ += `${key}: ${value},\r\n`;      
    }
    this.json_ += "}";
    this.state = {
      slideOpen: false,
      modalOpen: false,
      json: this.json_
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleSlideOpen() {
    const items = document.getElementsByClassName(`dropdown`);
    for (var idx = 0; idx < items.length; idx++) {
      items[idx].style.display = 'none'
    }
    this.setState({slideOpen: true});
  }

  handleSlideClose() {
    const items = document.getElementsByClassName(`dropdown`);
    for (var idx = 0; idx < items.length; idx++) {
      items[idx].style.display = 'block'
    }
    this.setState({slideOpen: false});
  }

  handleUpdateData() {
    const id = this.numberid;
    const json = this.state.json;

    if (id === '' || json === '') {
        toast.configure();
        toast.error("Please fill every items.", 4);
        return;
    }

    this.setState({slideOpen: false});
    this.props.object.updateDataItem(id, json);
  }

  handleModalOpen() {
    this.setState({modalOpen: true});
  }

  handleModalClose() {
    this.setState({modalOpen: false});
  }

  handleDeleteRow() {
    this.setState({modalOpen: false});
    this.props.object.deleteDataItem(this.props.item.id);
  }

  handleChange(e) {
    this.setState({json: e.target.value});
  }

  render() {
    const {slideOpen, modalOpen, json} = this.state;
    let modalClose = () => this.setState({ modalOpen: false });
    return (
      <DDE>
        <div className="dropdown" style={{width: "50px", textAlign: "center"}}>
          <FiMoreVertical style={{marginTop: '-4px', fontWeight: 'bold'}} className="table-edit" />
          <div className={`dropdown-content talk-bubble tri-right border round ${this.props.flagLastest ? 'right-bottom' : 'right-top'}`}>
            <div className="talktext">
              <p style={{color: '#617088'}} onClick={()=>this.handleSlideOpen()}>Edit</p>
            </div>
            <div className="talktext">
              <p style={{color: '#617088'}} onClick={()=>this.handleModalOpen()}>Delete</p>
            </div>
          </div>
        </div>
        <SlidingPane
          isOpen={slideOpen}
          title="EDIT DATA"
          from="right"
          width="250px"
          onRequestClose={() => this.handleSlideClose()}
        >
          <div className="row add-view-item-adjust">
              <label htmlFor="partition">JSON Data</label>
              <Form.Control 
                id="additem-json" 
                as="textarea" 
                rows={10} 
                style={{resize:'none'}} 
                value={ json ? String(json) : "" }
                onChange={this.handleChange}/>
          </div>                  
          <br/>                    
          <div className="row add-view-item-adjust">
            <Button variant="info" style={{width: '100%'}} onClick={()=>{this.handleUpdateData();}}>Update</Button>
          </div>
          <Button variant="light" style={{width: '100%'}} onClick={()=>this.handleSlideClose()}>Cancel</Button>
        </SlidingPane>
        <Modal
          show={modalOpen}
          onHide={modalClose}
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              ID = {this.numberid} Item
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h5>Do you really want to delete it?</h5>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="danger" onClick={() => this.handleDeleteRow()}>Delete</Button>
            <Button onClick={() => this.handleModalClose()}>Close</Button>
          </Modal.Footer>
        </Modal>
      </DDE>
    );
  }
}

export const DDE = styled.div`

.profile-icon{
    font-size: 16px;
    border: none;
    cursor: pointer;
    background-color: transparent;
  }
  
  .dropdown {
    position: relative;
    display: inline-block;
    z-index: 10;
  }
  
  .dropdown-content {
    cursor: pointer;
    display: none;
    position: absolute;
    right: 32px;
    background-color: #f9f9f9;
    min-width: 120px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 100;
    text-align: left;
  }
  
  .dropdown-content p {
    color: black;
    padding: 5px 16px;
    text-decoration: none;
    display: block;
  }
  
  .dropdown-content p:hover {
    background-color: #f1f1f1;
    border-radius: 10px;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
  }
  
  .dropdown:hover .dropdown-content {
    display: block;
    z-index: 100;
  }
  
  .dropdown:hover .profile-icon {
  
  }

  .talkbubble {
    width: 120px;
    height: 80px;
    background: red;
    position: relative;
    -moz-border-radius: 10px;
    -webkit-border-radius: 10px;
    border-radius: 10px;
  }
  .talkbubble:before {
    content: "";
    position: absolute;
    right: 100%;
    top: 26px;
    width: 0;
    height: 0;
    border-top: 13px solid transparent;
    border-right: 26px solid red;
    border-bottom: 13px solid transparent;
  }
  
  `;

export default withRouter(DropdownEdit);